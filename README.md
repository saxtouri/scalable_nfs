Scalable NFS
============

Create a logical volume and share it over NFS, on an IaaS.

Steps
-----
1. Provision a VM to be an NFS server, add it to inventory.yml
2. Edit logical_volume.yml to add the list of VMs and the
	physical volumes to merge into a logical one
3. You can also check roles/scalable_nfs/defaults/main.yml and
	override any other variable in logical_volume.yml
4. ansible-galaxy run logical_volume.yml
